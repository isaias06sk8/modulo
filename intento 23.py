import redis
import time
r_server = redis.Redis("localhost")

def insertar():
    p=input("\nIngrese una palabra: ")
    d=input(f"Ingrese la definicion de {p}: ")
    r_server.hset('diccionario', 'palabra', p)
    r_server.hset('diccionario', 'definicion', d)
    print("Palabra y definición añadida correctamente\n")

def editar():
    if r_server.hexists('diccionario', 'palabra')==True:
        pe = input("\nIngrese la palabra editada: ")
        r_server.hset('diccionario', 'palabra', pe)
        print("Palabra editada correctamente\n")
    else:
        print("No hay datos que mostrar")

def eliminar():
    if r_server.hexists('diccionario', 'palabra')==True:
        r_server.hdel('diccionario', 'palabra')
        r_server.hdel('diccionario', 'definicion')
        print("Palabra y definicion eliminada correctamente\n")
    else:
        print("No hay datos que mostrar")

def visualizar():
    print(r_server.hgetall('diccionario'))

def buscar():
    if r_server.hexists('diccionario', 'palabra')==True:
            print(r_server.hget('diccionario','palabra'))
            print(r_server.hget('diccionario','definicion'))
    else:
        print("No hay datos que mostrar")

op=0
while(op!=6):
    print('''MENU DE OPCIONES\n
            1) Agregar nueva palabra
            2) Editar una palabra existente
            3) Eliminar palabra existente
            4) Ver listado de palabras                
            5) Buscar significado de palabra
            6) Salir''')
    op=int(input("Ingrese la opcion: "))

    if (op==1):
        insertar()
        time.sleep(2)
    elif (op==2):
        editar()
        time.sleep(2)
    elif (op==3):
        eliminar()
        time.sleep(2)
    elif (op==4):
        visualizar()
        time.sleep(2)
    elif (op==5):
        buscar()
        time.sleep(2)
    elif (op==6):
        print("\nUsted ha salido")
        time.sleep(2)
    else:
        print("\nError. Dato no válido")
        time.sleep(2) 
